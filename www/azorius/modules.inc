MODGO_MODULES =	\
	github.com/gorilla/mux			 v1.8.0 \
	golang.org/x/crypto			 v0.10.0 \
	golang.org/x/image			 v0.0.0-20220413100746-70e8d0d3baa9 \
	golang.org/x/net			 v0.10.0 \
	golang.org/x/sys			 v0.9.0 \
	golang.org/x/term			 v0.9.0 \
	golang.org/x/text			 v0.10.0 \
	golang.org/x/tools			 v0.0.0-20180917221912-90fa682c2a6e \
	humungus.tedunangst.com/r/go-sqlite3	 v1.1.3 \
	humungus.tedunangst.com/r/webs		 v0.6.65
MODGO_MODFILES =	\
	golang.org/x/crypto	 v0.0.0-20220411220226-7b82a4e95df4 \
	golang.org/x/net	 v0.0.0-20211112202133-69e39bad7dc2 \
	golang.org/x/net	 v0.0.0-20220425223048-2871e0cb64e4 \
	golang.org/x/sys	 v0.0.0-20201119102817-f84b799fce68 \
	golang.org/x/sys	 v0.0.0-20210423082822-04245dca01da \
	golang.org/x/sys	 v0.0.0-20210615035016-665e8c7367d1 \
	golang.org/x/sys	 v0.0.0-20211216021012-1d35b9e2eb4e \
	golang.org/x/sys	 v0.8.0 \
	golang.org/x/term	 v0.0.0-20201126162022-7de9c90e9dd1 \
	golang.org/x/term	 v0.0.0-20210927222741-03fcf44c2211 \
	golang.org/x/term	 v0.8.0 \
	golang.org/x/text	 v0.3.6 \
	golang.org/x/text	 v0.3.7 \
	golang.org/x/text	 v0.9.0
